module Md1 where

{--
    Autors: Matīss Zērvēns
--}


{--
    Funkcija ļauj iegūt kompozīciju gadījumā,
    kad pirmais saraksts ir viens elements (attiecīgi x un y parametri)
--}
filterListByFst :: (Eq a) => a -> a -> [(a, a)] -> [(a, a)]
filterListByFst x y [] = []
filterListByFst x y (lHead:lTail) = let headFst = fst lHead
                                        headSnd = snd lHead
                                    in if headFst == y
                                       then (x, headSnd) : filterListByFst x y lTail
                                       else filterListByFst x y lTail


{--
    Izņem visus pārus no saraksta, kuri sakrīt ar padotā pāra vērtībām
--}
removeDuplicate :: (Eq a) => (a, a) -> [(a, a)] -> [(a, a)]
removeDuplicate _ [] = []
removeDuplicate (f, s) ((hf, hs):xs) = if f == hf &&  s == hs
                                       then removeDuplicate (f, s) xs
                                       else (hf, hs) : removeDuplicate (f, s) xs


{--
    Izņem vienādos pārus no saraksta, tiek saglabāti pirmie unikālie pāri

    Atkarības:
        removeDuplicate
--}
removeDuplicatesInList :: (Eq a) => [(a, a)] -> [(a, a)]
removeDuplicatesInList [] = []
removeDuplicatesInList ((fX, sX):xs) = (fX, sX) : removeDuplicatesInList (removeDuplicate (fX, sX) xs)


{--
    Bāzes gadījumā rezultāts ir [], ja kāda no vārdnīcām ir tukša
    Pretējā gadījumā kamēr pirmais saraksts nav tukšs:
        iegūst galvas elementa kompozīciju ar otro sarakstu izsaucot funkciju 'filterListByFst', kuras rezultātu
        sakonkatinē ar šīs funkcijas (cc) rezultātu padodot pirmā saraksta asti un otro sarakstu

    Atkarības:
        removeDuplicatesInList
        filterListByFst
--}
cc :: (Eq a) => [(a, a)] -> [(a, a)] -> [(a, a)]
cc [] _ = []
cc _ [] = []
cc (x:xs) b = let hFst = fst x
                  hScnd = snd x
              in removeDuplicatesInList ((filterListByFst hFst hScnd b) ++ cc xs b)



{--
    Izņem no saraksta visas vērtības, kas sakrīt ar padotā elementa vērtību
--}
removeDuplicateLinkedList :: (Eq a) => a -> [a] -> [a]
removeDuplicateLinkedList _ [] = []
removeDuplicateLinkedList v (x:xs) = if v == x
                                     then removeDuplicateLinkedList v xs
                                     else x : removeDuplicateLinkedList v xs

{--
    Izņem no saraksta vienādos elementus, tiek saglabāti pirmie unikālie elementi

    Atkarības:
        removeDuplicateLinkedList
--}
removeDuplicatesInLinkedList :: (Eq a) => [a] -> [a]
removeDuplicatesInLinkedList [] = []
removeDuplicatesInLinkedList (x:xs) = x : removeDuplicatesInLinkedList (removeDuplicateLinkedList x xs)



{--
    Funkcijas atgriež sarakstu, kurā galvas elements reprezentē atslēgas vērtību.
    Ja atslēga netika atrasta, tad tiks atgriezts tukšs saraksts
--}
findFirstTranslation :: (Eq a) => a -> [(a, a)] -> [a]
findFirstTranslation _ [] = []
findFirstTranslation key ((headKey, headVal) : t) = if key == headKey
                                                    then [headVal]
                                                    else findFirstTranslation key t



{--
    Bāzes gadījumā rezultāts ir [], ja saraksts ir tukšs vai
    vārdnīca ir tukša.
    Pretējā gadījumā kamēr tulkojamais saraksts nav tukšs:
        atrod saraksta galvas tulkojumu izmantojot funkciju 'findFirstTranslation',
        ja tulkojums netika atrasts, tad atgriež rezultātu tulkojot saraksta asti
        pretējā gadījumā tulkojumu konkatinē ar šīs funkcijas izsaukumu padodot saraksta asti un vārdnīcu

    Tiek pieņemts, ka vārdnīcā nevar būt vienam vārdam vairāk par vienu tulkojumu (findFirstTranslation)

    Atkarības:
        findFirstTranslation
        removeDuplicatesInLinkedList

--}
appd :: (Eq a) => [a] -> [(a, a)] -> [a]
appd [] _ = []
appd _ [] = []
appd (entry:entries) dict = let translation = findFirstTranslation entry dict
                            in if length translation == 0
                               then appd entries dict
                               else removeDuplicatesInLinkedList (head translation : appd entries dict)







{--
    Funkcija veic kompozīciju ar vārdnīcām (pirmais un otrais parametrs) tik ilgi,
    līdz viena no tām kļūst par tukšu sarakstu vai kompozīcija apiet pilnu riņķi.

    Pēc katras vārdnīcu kompozīcijas, kompozīcijas rezultāts tiek pievienots pie rezultāta vārnīcas (trešais parametrs),
    tiek izpildīta nākamā rekursija, kurai padod pirmo vārdnīcu, kompozīcijas rezultāta vārdnīcu, jauno rezultāta vārdnīcu

--}
composeTillEmpty :: (Eq a) => [(a, a)] -> [(a, a)] -> [(a, a)] -> [(a, a)]
composeTillEmpty [] _ rez = rez
composeTillEmpty _ [] rez = rez
composeTillEmpty base composition rez = let nextComp = cc base composition
                                        in if composition == nextComp || base == nextComp
                                           then rez
                                           else composeTillEmpty base nextComp (rez ++ nextComp)



{--
    Realizē vārdnīcas tranzitīvo slēgumu
    Ja vārdnīca nav tukša, tad rezultāts tiek iegūts konkatinējot sākotnējo vārnīcu ar papildus vārdnīcu, kuru ģenerē
    funkcija 'composeTillEmpty'

    Atkarības:
        removeDuplicatesInList
        composeTillEmpty
--}
ttt :: (Eq a) => [(a, a)] -> [(a, a)]
ttt [] = []
ttt startDict = removeDuplicatesInList (startDict ++ composeTillEmpty startDict startDict [])






-- Testēšanas funkcijas

cc1 :: [([Char], [Char])]
cc1 = cc firstDict secondDict
    where firstDict = [("a", "b"), ("", "x"), ("y", "r"), ("bab", "ab"), ("a", "x")]
          secondDict = [("b", "a"), ("x", "yy"), ("r", "rr"), ("ab", "bab"), ("x", "zzz"), ("c", "z")]



cc2 :: [(Int, Int)]
cc2 = cc firstDict secondDict
    where firstDict = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 0)]
          secondDict = [(4, 3), (3, 2), (2, 1), (1, 0), (0, 4)]





appd1 :: [[Char]]
appd1 = appd list dict
      where list = ["viens", "plus", "divi", "ir", "=", "trīs", "vai", ">", "trīs", "seši"]
            dict = [("viens", "one"), ("divi", "two"), ("trīs", "three"), ("četri", "four"), ("pieci", "five"), ("seši", "six")]



appd2 :: [Integer]
appd2 = appd list dict
      where list = [(10^100), 18, 120, 24, 1, 13, 2]
            dict = [((10^100), 70), (120, 5), (1, 0), (24, 4), (2, 2)]




ttt1 :: [(Char, Char)]
ttt1 = ttt dict
     where dict = [('a', 'b'), ('b', 'c'), ('c', 'd'), ('d', 'e'), ('e', 'a')]


ttt2 :: [([Char], [Char])]
ttt2 = ttt dict
     where dict = [("a", "b"), ("b", "c"), ("c", "d"), ("d", "e"), ("e", "f")]