module Main where
import Md2

{--
    Izpilda ff_a, ff_b, ff_c uz koku 'testTree' un izdrukā rezultātus konsolē
--}
main :: IO()
main = do
       let test_ff_a = treeToPreOrderList(ff_a testTree)
           test_ff_b = treeToPreOrderList(ff_b testTree)
           test_ff_c = treeToPreOrderList(ff_c testTree)
           unmodified = treeToPreOrderList(testTree)
        in do putStrLn "Funkcijas ff_a rezultāts uz koku 'testTree'"
              putStrLn "Sākotnējais koks: "
              print unmodified
              putStrLn "ff_a rezultāts: "
              print test_ff_a

              putStrLn ""

              putStrLn "Funkcijas ff_b rezultāts uz koku 'testTree'"
              putStrLn "Sākotnējais koks: "
              print unmodified
              putStrLn "ff_b rezultāts: "
              print test_ff_b

              putStrLn ""

              putStrLn "Funkcijas ff_c rezultāts uz koku 'testTree'"
              putStrLn "Sākotnējais koks: "
              print unmodified
              putStrLn "ff_c rezultāts: "
              print test_ff_c
