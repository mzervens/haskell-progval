module Md2 where

{-- Koka struktūra, kur mezgls var būt tukšais elements vai elements,
    kas sastāv no saraksta, kreisā bērna un labā bērna
--}
data TTT aa = Null
            | Node {
                        val :: [aa],
                        left :: TTT aa,
                        right :: TTT aa
                    }


-- x = x^2
a :: Integer -> Integer
a val = val * val


-- x = x % 5
b :: Integer -> Integer
b val = val `mod` 5


{--
    Padoto parametru reizina ar sarakstu,
    saskaita rezultējošā saraksta elementu summu
    [1, 3, 5, 7, 9] * x = x + 3x + 5x + 7x + 9x
--}
c :: Integer -> Integer
c val = let list = [1, 3..10]
            rezList = map (\x -> x * val) list
        in sum rezList



{--
    map funkcijas apakšgadījums (map spēj vairāk)
--}
map' :: (aa -> aa) -> [aa] -> [aa]
map' _ [] = []
map' f (x:xs) = (f x) : (map' f xs)




mm :: (aa -> aa) -> (TTT aa) -> (TTT aa)
mm _ Null = Null
mm f treeNode = Node (map' f (val treeNode)) (mm f (left treeNode)) (mm f (right treeNode))


ff_a :: (TTT Integer) -> (TTT Integer)
ff_a tree = (mm a tree)


ff_b :: (TTT Integer) -> (TTT Integer)
ff_b tree = (mm b tree)


ff_c :: (TTT Integer) -> (TTT Integer)
ff_c tree = (mm c tree)




{--
    Izveido sarakstu no koka mezglu datiem preorder secībā
    testTree gadījumā iegūst [[0,-1,2],[3,5,-2],[7],[8,88],[-9],[12],[],[1],[19]]
--}
treeToPreOrderList :: (TTT aa) -> [[aa]]
treeToPreOrderList Null = []
treeToPreOrderList tree = (val tree) : ((treeToPreOrderList (left tree)) ++ (treeToPreOrderList (right tree)))


{--
    Testa koka shēma

                                    [0, -1, 2]
                    [3, 5, -2]                          []

            [7]                 [12]                [1]     [19]

    [8, 88]         [-9]
--}
testTree = Node {
                    val = [0, -1, 2],

                    left = Node{
                                    val = [3, 5, -2],
                                    left = Node{

                                                    val = [7],
                                                    left = Node{
                                                                    val = [8, 88],
                                                                    left = Null,
                                                                    right = Null
                                                                },
                                                    right = Node{

                                                                    val = [-9],
                                                                    left = Null,
                                                                    right = Null

                                                    }

                                                },
                                    right = Node{val = [12], left = Null, right = Null}
                                },


                    right = Node{
                                    val = [],
                                    left = Node{val = [1], left = Null, right = Null},
                                    right = Node{val = [19], left = Null, right = Null}

                                    }
}

